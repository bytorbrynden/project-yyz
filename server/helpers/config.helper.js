/**
 * yyz/server/helpers/config.helper.js
 * 
 * Contains all config-related helper logic.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var fs = require("fs");
var stripComments = require("./json_comment_stripper.helper.js");

var _getAllDatasets = function(config) {
    return function() {
        var allDatasets = [];
        
        for (var dataset in config.datasets) {
            if (!config.datasets[dataset].hide) {
                allDatasets.push({
                    "name": dataset,
                    "description": config.datasets[dataset].description || ""
                });
            }
        }
        
        return allDatasets;
    };
};

var _getDatasetNames = function(config) {
    return function() {
        return Object.keys(config.datasets);
    };
};

var _configHasDataset = function(config) {
    return function(dataset) {
        return config.datasets.hasOwnProperty(dataset);
    };
};

var _getDatasetConfig = function(config) {
    return function(dataset) {
        return config.datasets[dataset];
    };
};

var _getDatasetSourceMeta = function(config) {
    return function(dataset) {
        var srcMeta = { "dataType": null, "locationType": null };
        var source = _getDatasetConfig(config)(dataset).src;
        
        if (/^http(s)?:\/\//.test(source)) srcMeta.locationType = "external";
        else srcMeta.locationType = "local";
         
        if (/\.db$/.test(source)) srcMeta.dataType = "sqlite";
        else srcMeta.dataType = "json";
        
        return srcMeta;
    };
};

module.exports = function(pathToConfig) {
    var config = JSON.parse(stripComments(fs.readFileSync(pathToConfig)
        .toString()));
    
    return {
        "getAllDatasets"      : _getAllDatasets(config),
        "getDatasetNames"     : _getDatasetNames(config),
        "configHasDataset"    : _configHasDataset(config),
        "getDatasetConfig"    : _getDatasetConfig(config),
        "getDatasetSourceMeta": _getDatasetSourceMeta(config)
    };
};