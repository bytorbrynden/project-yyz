/**
 * yyz/server/helpers/json_comment_stripper.helper.js
 * 
 * Strips C-Style comments from JSON strings.
 * Adapted from https://github.com/getify/JSON.minify/blob/javascript/minify.json.js
 * 
 * Created by Kyle "getify" Simpson on 06/12/2015
 * 
 * Released under a(n) MIT License
 */
module.exports = function(n){var t,e,r,g,s=/"|(\/\*)|(\*\/)|(\/\/)|\n|\r/g,i=!1,l=!1,x=!1,a=[],o=0,c=0;for(s.lastIndex=0;t=s.exec(n);)r=RegExp.leftContext,g=RegExp.rightContext,l||x||(e=r.substring(c),i||(e=e.replace(/(\n|\r|\s)*/g,"")),a[o++]=e),c=s.lastIndex,'"'!=t[0]||l||x?"/*"!=t[0]||i||l||x?"*/"!=t[0]||i||!l||x?"//"!=t[0]||i||l||x?"\n"!=t[0]&&"\r"!=t[0]||i||l||!x?l||x||/\n|\r|\s/.test(t[0])||(a[o++]=t[0]):x=!1:x=!0:l=!1:l=!0:(e=r.match(/(\\)*$/),i&&e&&e[0].length%2!=0||(i=!i),c--,g=n.substring(c));return a[o++]=g,a.join("")}; // eslint-disable-line