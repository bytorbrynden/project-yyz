/**
 * yyz/server/routes/api.js
 * 
 * Defines all API-related server routes.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var express = require("express");
var path    = require("path");
var fs      = require("fs");
var router  = express.Router();

// TODO: Don't hardcode config-file path
var configHelper  = require("../helpers/config.helper.js")(path.resolve("./data/config.json"));
var dataHelper    = require("../helpers/data.helper.js")();
var dataProviders = require("../data_providers/main.js");

router.get("/", function(request, response) {
    // TODO: Present API documentation?
    response.send("Api Root");
});

router.get("/datasets", function(request, response) {
    response.json(configHelper.getAllDatasets());
});

router.get("/dataset/:dataset", function(request, response) {
    var datasetName   = request.params.dataset;
    var datasetMeta   = null;
    var dataProvider  = null;
    var datasetConfig = null;
    
    if (!configHelper.configHasDataset(datasetName)) {
        response.status(500); // HTTP 500; Internal Server Error
        response.json({ "error": "Unknown dataset \"" + datasetName + "\"!" });
        
        return;
    }
    
    datasetMeta   = configHelper.getDatasetSourceMeta(datasetName);
    dataProvider  = dataProviders[datasetMeta.dataType];
    datasetConfig = configHelper.getDatasetConfig(datasetName);
    
    dataHelper.getData(datasetConfig.src, datasetMeta, function(data) {
        var dataset = dataProvider(datasetConfig, data).get();
        
        dataset.name = datasetName;
        
        response.json(dataset);
    });
});

router.get("/dataset/:dataset/:table", function(request, response) {
    var datasetName = request.params.dataset;
    var tableName   = request.params.table;
    var datasetMeta   = null;
    var dataProvider  = null;
    var datasetConfig = null;
    
    if (!configHelper.configHasDataset(datasetName)) {
        response.status(500); // HTTP 500; Internal Server Error
        response.json({ "error": "Unknown dataset \"" + datasetName + "\"!" });
        
        return;
    }
    
    datasetMeta   = configHelper.getDatasetSourceMeta(datasetName);
    dataProvider  = dataProviders[datasetMeta.dataType];
    datasetConfig = configHelper.getDatasetConfig(datasetName);
    
    dataHelper.getData(datasetConfig.src, datasetMeta, function(data) {
        response.json({
            "parent": datasetName,
            "self"  : tableName,
            "data"  : dataProvider(datasetConfig, data).tables.get(tableName)
        });
    });
});

router.get("/stylesheet/:dataset", function(request, response) {
    var datasetName   = request.params.dataset;
    var datasetConfig = null;
    
    response.set("Content-Type", "text/css");
    
    if (!configHelper.configHasDataset(datasetName)) {
        return response.send("");
    }
    
    datasetConfig = configHelper.getDatasetConfig(datasetName);
    
    if (datasetConfig.hasOwnProperty("stylesheet")) {
        fs.readFile(path.resolve(datasetConfig.stylesheet), function(error, data) {
            if (error) {
                response.send("");
                
                return console.log("Unable to open file at path \"" +
                    datasetConfig.stylesheet + "\"");
            }
            
            response.send(data);
        });
    } else {
        response.send("");
    }
});

module.exports = router;