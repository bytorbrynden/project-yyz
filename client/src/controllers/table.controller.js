/**
 * yyz/client/controllers/table.controller.js
 * 
 * Defines our table controller's logic.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/04/2016
 */
var arrayToObject = function(array, value) {
    var obj = {};
    
    for (var item in array) {
        obj[array[item]] = value;
    }
    
    return obj;
};

var filterStringsToObject = function(filtersString) {
    var filtersObj = {};
    
    var arrayOfFilterStrs = filtersString.split(",");
    
    for (var filterStringIndex = 0; filterStringIndex < arrayOfFilterStrs.length; filterStringIndex++) {
        var filterStr = arrayOfFilterStrs[filterStringIndex];
        
        if (filterStr !== "") {
            var filterName = filterStr.split("-")[0];
            var filterValue = filterStr.substr(filterName.length + 1);
            
            if (!filtersObj.hasOwnProperty(filterName)) {
                filtersObj[filterName] = {};
            }
            
            filtersObj[filterName][filterValue] = true;
        }
    }
    
    return filtersObj;
};

var filterStringsFromObject = function(filtersObject) {
    var filterStrings = "";
    
    var writtenFilter = false;
    
    for (var filter in filtersObject) {
        var filterNotChanged = true;
        
        for (var value in filtersObject[filter]) {
            filterNotChanged = (filterNotChanged && filtersObject[filter][value]);
        }
        
        if (!filterNotChanged) {
            for (var value in filtersObject[filter]) {
                if (filtersObject[filter][value]) {
                    filterStrings += ((writtenFilter ? ',' : '') + filter + "-" + value);
                    
                    writtenFilter = true;
                }
            }
        }
    }
    
    return filterStrings;
};

var sortsObjectFromSortString = function(sortsString) {
    var sortsObject = {};
    
    var arrayOfSortStrs = sortsString.split(",");
    
    for (var sortIndex = 0; sortIndex < arrayOfSortStrs.length; sortIndex++) {
        var sortString = arrayOfSortStrs[sortIndex].replace(/\s/gi, "_");
        var isReversed = (sortString[0] === "-");
        var sortName = (isReversed ? sortString.substr(1) : sortString);
        
        sortsObject[sortName] = (isReversed ? "-" : "");
    }
    
    return sortsObject;
};

var sortStringsFromSortsObject = function(sortsObject) {
    var sortStrings = "";
    
    var writtenSort = false;
    
    for (var sort in sortsObject) {
        sortStrings += ((writtenSort ? ',' : '') + sortsObject[sort] + sort);
        
        writtenSort = true;
    }
    
    return sortStrings;
};

module.exports = ["markdown", "$window", "$location", "$http", "$scope", function(markdown, $window, $location, $http, $scope) {
    $scope.table  = { };
    $scope.loaded = false;
    
    $scope.searchTerm = "";
    $scope.filters    = { };
    $scope.sorts      = { };
    
    $scope.urlParameters = $location.search();
    
    $http({"method": "GET", "url": window.yyzApiUrl}).then(
        function(response) {
            var table = response.data;
            
            var filtersToEnable = { };
            var columnsToEnable = [ ];
            
            if ($scope.urlParameters.hasOwnProperty("search")) {
                $scope.searchTerm = $scope.urlParameters.search;
            }
            
            if ($scope.urlParameters.hasOwnProperty("filters") && (typeof $scope.urlParameters.filters) === "string") {
                filtersToEnable = filterStringsToObject($scope.urlParameters.filters);
            }
            
            if ($scope.urlParameters.hasOwnProperty("sorts") && (typeof $scope.urlParameters.sorts) === "string") {
                $scope.sorts = sortsObjectFromSortString($scope.urlParameters.sorts);
            }
            
            if ($scope.urlParameters.hasOwnProperty("columns") && (typeof $scope.urlParameters.columns) === "string") {
                if ($scope.urlParameters.columns.length > 0)
                    columnsToEnable = $scope.urlParameters.columns.split(",");
            }
            
            for (var filter in table.data.filters) {
                if (!filtersToEnable.hasOwnProperty(filter)) {
                    $scope.filters[filter] = arrayToObject(table.data.filters[filter], true);
                } else {
                    for (var value in table.data.filters[filter]) {
                        var filterValue = table.data.filters[filter][value];
                        
                        if (!$scope.filters.hasOwnProperty(filter)) {
                            $scope.filters[filter] = { };
                        }
                        
                        if (!filtersToEnable[filter].hasOwnProperty(filterValue)) {
                            $scope.filters[filter][filterValue] = false;
                        } else {
                            $scope.filters[filter][filterValue] = true;
                        }
                    }
                }
            }
            
            for (var sort in table.data.sorts) {
                var sortString = table.data.sorts[sort].replace(/\s/gi, "_");
                var isReversed = (sortString[0] === "-");
                var sortName = (isReversed ? sortString.substr(1) : sortString);
                
                if (!$scope.sorts.hasOwnProperty(sortName)) {
                    $scope.sorts[sortName] = (isReversed ? "-" : "");
                }
            }
            
            for (var colIdent in table.data.cols) {
                var visible = true;
                
                if (columnsToEnable.length > 0 && columnsToEnable.indexOf(colIdent) === -1)
                    visible = false;
                
                table.data.cols[colIdent] = {
                    "internal": table.data.cols[colIdent].internal,
                    "external": table.data.cols[colIdent].external,
                    "visible" : visible
                };
            }
            
            $scope.tableFlags = arrayToObject(table.data.flags, true);
            
            $scope.table   = table;
            $scope.loaded  = true;
            
            console.info("Dataset name: %s", $scope.table.parent);
            console.info("Table name: %s", $scope.table.self);
        },
        function(error) {
            console.error(error);
        }
    );
    
    $scope.tableSearchDelegate = function(searchTerm) {
        $location.search("search", searchTerm);
    };
    
    $scope.tableFilterDelegate = function(elem) {
        for (var filter in $scope.filters) {
            var filterIdent = filter;
            
            if (!elem.hasOwnProperty(filterIdent)) {
                continue;
            }
            
            if (Array.isArray(elem[filterIdent])) {
                for (var elFilter in elem[filterIdent]) {
                    if ($scope.filters[filter][ elem[filterIdent][elFilter] ]) {
                        return true;
                    }
                }
                
                return false;
            } else {
                if ($scope.filters[filter].hasOwnProperty(elem[filterIdent]) && !$scope.filters[filter][elem[filterIdent]]) {
                    return false;
                }
            }
        }
        
        return true;
    };
    
    $scope.tableSortDelegate = function() {
        var tmpSorts = [];
        
        for (var sort in $scope.sorts) {
            tmpSorts.push($scope.sorts[sort] + sort);
        }
        
        return tmpSorts;
    };
    
    $scope.updateSort = function(columnName) {
        if ($scope.sorts.hasOwnProperty(columnName)) {
            if ($scope.sorts[columnName] === "-")
                $scope.sorts[columnName] = "";
            else if ($scope.sorts[columnName] === "")
                delete $scope.sorts[columnName];
        } else {
            $scope.sorts[columnName] = "-";
        }
        
        $location.search("sorts", "");
        $location.search("sorts", sortStringsFromSortsObject($scope.sorts));
    };
    
    $scope.updateFilters = function() {
        $location.search("filters", filterStringsFromObject($scope.filters));
    };
    
    $scope.updateColumns = function() {
        var visibleColumns = [ ];
        
        for (var column in $scope.table.data.cols) {
            if ($scope.table.data.cols[column].visible)
                visibleColumns.push(column);
        }
        
        $location.search("columns", visibleColumns.join(","));
    };
    
    $scope.tableHasFilters = function() {
        return Object.keys($scope.filters).length > 0;
    };
    
    $scope.selectAll = function(filter) {
        for (var prop in $scope.filters[filter]) {
            $scope.filters[filter][prop] = true;
        }
    };
    
    $scope.deselectAll = function(filter) {
        for (var prop in $scope.filters[filter]) {
            $scope.filters[filter][prop] = false;
        }
    };
    
    $scope.showAllColumns = function() {
        for (var column in $scope.table.data.cols) {
            $scope.table.data.cols[column].visible = true;
        }
        
        $location.search("columns", "");
    };
    
    $scope.hideAllColumns = function() {
        for (var column in $scope.table.data.cols) {
            $scope.table.data.cols[column].visible = false;
        }
    };
    
    $scope.formatContent = function(content) {
        return markdown.format($scope.table.data.flags, content);
    };
}];