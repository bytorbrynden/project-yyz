#!/bin/bash
# Copies production files to specified target

PROJECT_ROOT="$1"
TARGET_DIRECTORY="$2"

COPY_FLAGS="-r"

ITEMS_TO_INCLUDE=(
    "$PROJECT_ROOT/server.js"
    "$PROJECT_ROOT/client"
    "$PROJECT_ROOT/server"
    "$PROJECT_ROOT/package.json"
)

copy_items() {
    echo "Copying items"

    for ITEM_TO_INCLUDE in ${ITEMS_TO_INCLUDE[@]}
    do
	echo "Copying $ITEM_TO_INCLUDE to $TARGET_DIRECTORY"
	cp $COPY_FLAGS $ITEM_TO_INCLUDE $TARGET_DIRECTORY
    done
}

if [ -d "$TARGET_DIRECTORY" ]
then
    copy_items
else
    echo "Unable to find directory at path \"$TARGET_DIRECTORY\", create it? (y/n)"

    read CREATE_DIRECTORY

    if [ $CREATE_DIRECTORY = "y" ]
    then
	echo "Creating directory at path \"$TARGET_DIRECTORY\""
	mkdir $TARGET_DIRECTORY

	copy_items
    fi
fi

echo "All done"
