/**
 * yyz/client/controllers/root.controller.js
 * 
 * Defines our root controller's logic.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/03/2016
 */
module.exports = ["$http", "$scope", function($http, $scope) {
    $scope.datasets = [ ];
    
    $http({"method": "GET", "url": "/api/datasets"}).then(
        function(response) {
            $scope.datasets = response.data;
        },
        function(error) {
            console.error(error);
        }
    );
}];