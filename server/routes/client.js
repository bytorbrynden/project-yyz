/**
 * yyz/server/routes/client.js
 * 
 * Defines all client-related server routes.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var express = require("express");
var path    = require("path");
var router  = express.Router();

var getServerUrl = function(request) {
    return ("//") + request.get("Host");
};

router.get("/", function(request, response) {
    response.sendFile(path.resolve("./client/templates/root.template.html"));
});

router.get("/:dataset", function(request, response) {
    var datasetName = request.params.dataset.replace(/\s/gi, "%20");
    var apiUrl  = (getServerUrl(request) + ("/api/dataset/" + datasetName));
    
    response.set("YYZ-API-URL", apiUrl);
    response.sendFile(path.resolve("./client/templates/dataset.template.html"));
});

router.get("/:dataset/:table", function(request, response) {
    var datasetName = request.params.dataset.replace(/\s/gi, "%20");
    var tableName   = request.params.table.replace(/\s/gi, "%20");
    var apiUrl = (getServerUrl(request) + ("/api/dataset/" + datasetName + "/"
        + tableName));
    var stylesheetUrl = (getServerUrl(request) + ("/api/stylesheet/" +
        datasetName));
    
    response.set("YYZ-API-URL", apiUrl);
    response.set("YYZ-STYLESHEET-URL", stylesheetUrl);
    response.sendFile(path.resolve("./client/templates/table.template.html"));
});

module.exports = router;