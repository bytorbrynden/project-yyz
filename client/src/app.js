/**
 * yyz/client/app.js
 * 
 * The entry-point for the yyz client application.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/03/2016
 */
var app = angular.module("yyz", ["ngSanitize"]);

// https://github.com/angular/angular.js/issues/1521
app.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
});

app.service("markdown", require("./services/markdown.service.js"));

app.controller("root.controller", require("./controllers/root.controller.js"));
app.controller("dataset.controller", require("./controllers/dataset.controller.js"));
app.controller("table.controller", require("./controllers/table.controller.js"));