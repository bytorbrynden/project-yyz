/**
 * yyz/server/data_providers/sqlite.provider.js
 * 
 * Defines the logic for our SQLite data-provider.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/04/2016
 */
var sqlite = require("sql.js").Database;

var dataProvider;
var buildQuery;
var setupDataset;
var setupTable;
var getTableRows;
var normalizeRows;
var getTableCols;
var getTableFilters;

dataProvider = function(datasetConfig, sourceData) {
    var dataset = setupDataset(new sqlite(sourceData), datasetConfig);
    
    return {
        "get": function() {
            return dataset;
        },
        "tables": {
            "get": function(table) {
                return (table ? dataset.tables[table] : dataset.tables);
            }
        }
    };
};

buildQuery = function(sqlTable, sqlColumns) {
    var queryStatement = "";
    var hasColumnWritten = false;
    
    queryStatement += "SELECT ";
    
    for (var sqlColumn in sqlColumns) {
        var firstChar = (hasColumnWritten ? ',' : '');
        
        queryStatement += firstChar + sqlTable + ".";
        queryStatement += sqlColumn + " AS " + sqlColumns[sqlColumn];
        
        hasColumnWritten = true;
    }
    
    queryStatement += " FROM " + sqlTable;
    
    return queryStatement;
};

setupDataset = function(inputDb, config) {
    var dataset = { "tables": { } };
    
    for (var table in config.tables) {
        dataset.tables[table] = setupTable(inputDb, config.tables[table]);
    }
    
    // TODO: Joins...
    
    return dataset;
};

setupTable = function(inputDb, tableConfig) {
    var table = {
        "cols"   : { },
        "rows"   : [ ],
        "flags"  : [ ],
        "sorts"  : [ ],
        "filters": { }
    };
    var sqlQuery = "";
    
    if (!tableConfig.hasOwnProperty("query")) {
        var sqlTable = Object.keys(tableConfig.data)[0];
        sqlQuery = buildQuery(sqlTable, tableConfig.data[sqlTable]);
    } else {
        sqlQuery = tableConfig.query;
    }
    
    table.rows = getTableRows(inputDb, sqlQuery);
    table.cols = getTableCols(table.rows);
    table.rows = normalizeRows(table.rows);
    
    if (tableConfig.hasOwnProperty("flags"))
        table.flags = tableConfig.flags;
    
    if (tableConfig.hasOwnProperty("sorts"))
        table.sorts = tableConfig.sorts;
        
    if (tableConfig.hasOwnProperty("filters"))
        table.filters = getTableFilters(table.rows, tableConfig.filters);
    
    return table;
};

getTableRows = function(inputDb, query) {
    var sqlQueryResults = inputDb.exec(query);
    var rows = [ ];
    
    sqlQueryResults.forEach(function(result) {
        result.values.forEach(function(value) {
            var row = { };
            
            value.forEach(function(colValue, colIndex) {
                row[result.columns[colIndex]] = colValue;
            });
            
            rows.push(row);
        });
    });
    
    return rows;
};

normalizeRows = function(tableRows) {
    var newTableRows = [];
    
    tableRows.forEach(function(row) {
        var newRow = {};
        
        for (var column in row) {
            newRow[column.replace(/\s/gi, "_")] = row[column];
        }
        
        newTableRows.push(newRow);
    });
    
    return newTableRows;
};

getTableCols = function(tableRows) {
    var cols = {};
    
    tableRows.forEach(function(row) {
        for (var col in row) {
            if (!cols.hasOwnProperty(col)) {
                var internal = col.replace(/\s/gi, "_");
                var external = col;
                
                cols[internal] = {
                    "internal": internal,
                    "external": external
                };
            }
        }
    });
    
    return cols;
};

getTableFilters = function(tableRows, columnsToFilter) {
    var filters = { };
    
    tableRows.forEach(function(row) {
        columnsToFilter.forEach(function(col) {
            col = col.replace(/\s/gi, "_");
            
            if (!filters.hasOwnProperty(col)) filters[col] = [ ];
            
            if (row.hasOwnProperty(col) &&
                filters[col].indexOf(row[col] === -1)) {
                filters[col].push(row[col]);
            }
            
            filters[col] = filters[col].sort();
        });
    });
    
    return filters;
};

module.exports = dataProvider;