/**
 * yyz/server/data_providers/json.provider.js
 * 
 * Defines the logic for our JSON data-provider.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var dataProvider;
var setupDataset;
var setupTable;
var getTableRows;
var normalizeRows;
var getTableCols;
var getTableFilters;

dataProvider = function(datasetConfig, sourceData) {
    var dataset = setupDataset(sourceData, datasetConfig);
    
    return {
        "get": function() {
            return dataset;
        },
        "tables": {
            "get": function(table) {
                return (table ? dataset.tables[table] : dataset.tables);
            }
        }
    };
};

setupDataset = function(input, config) {
    var dataset = { "tables": { } };
    
    for (var table in config.tables) {
        dataset.tables[table] = setupTable(input, config.tables[table]);
    }
    
    // TODO: Joins...
    
    return dataset;
};

setupTable = function(input, tableConfig) {
    var table = {
        "cols"   : { },
        "rows"   : [ ],
        "flags"  : [ ],
        "sorts"  : [ ],
        "filters": { }
    };
    
    table.rows = getTableRows(input, tableConfig.data);
    table.rows = Array.isArray(table.rows) ? table.rows : [ table.rows ];
    table.cols = getTableCols(table.rows);
    table.rows = normalizeRows(table.rows);
    
    
    if (tableConfig.hasOwnProperty("flags"))
        table.flags = tableConfig.flags;
    
    if (tableConfig.hasOwnProperty("sorts"))
        table.sorts = tableConfig.sorts;
    
    if (tableConfig.hasOwnProperty("filters"))
        table.filters = getTableFilters(table.rows, tableConfig.filters);
    
    return table;
};

getTableRows = function(inputData, dataConfig) {
    if (typeof inputData === "string") return inputData;
    if (inputData === undefined || inputData === null) return { };
    var outputData = { };
    
    for (var column in dataConfig) {
        if (typeof dataConfig[column] === "string") {
            if (inputData.hasOwnProperty(column)) {
                outputData[dataConfig[column]] = inputData[column];
            } else if (Array.isArray(inputData)) {
                outputData = [ ];
                
                inputData.forEach(function(item) {
                    outputData = outputData.concat(getTableRows(item,
                        dataConfig));
                });
            } else {
                outputData = [ ];
                
                for (var prop in inputData) {
                    outputData.push(getTableRows(inputData[prop], dataConfig));
                }
            }
        } else if (typeof dataConfig[column] === "object") {
            if (inputData.hasOwnProperty(column)) {
                var localData = getTableRows(inputData[column], dataConfig[column]);
                
                if (Array.isArray(localData)) {
                    outputData = [ outputData ];
                    
                    for (var localItem in localData) {
                        outputData.push(localData[localItem]);
                    }
                } else {
                    for (var localProp in localData) {
                        if (!outputData.hasOwnProperty(localProp)) {
                            outputData[localProp] = localData[localProp];
                        }
                    }
                }
            } else if (Array.isArray(dataConfig[column])) {
                outputData[column] = [ ];
                
                if (inputData.hasOwnProperty(column)) {
                    if (dataConfig[column].length === 1) {
                        var targetProp = dataConfig[column][0];
                        
                        inputData.forEach(function(item) {
                            outputData[column].push(item[targetProp]);
                        });
                    }
                }
            }
        }
    }
    
    return outputData;
};

normalizeRows = function(tableRows) {
    var newTableRows = [];
    
    tableRows.forEach(function(row) {
        var newRow = {};
        
        for (var column in row) {
            newRow[column.replace(/\s/gi, "_")] = row[column];
        }
        
        newTableRows.push(newRow);
    });
    
    return newTableRows;
};

getTableCols = function(tableRows) {
    var cols = {};
    
    tableRows.forEach(function(row) {
        for (var col in row) {
            if (!cols.hasOwnProperty(col)) {
                var internal = col.replace(/\s/gi, "_");
                var external = col;
                
                cols[internal] = {
                    "internal": internal,
                    "external": external
                };
            }
        }
    });
    
    return cols;
};

getTableFilters = function(tableRows, columnsToFilter) {
    var filters = { };
    
    tableRows.forEach(function(row) {
        columnsToFilter.forEach(function(col) {
            col = col.replace(/\s/gi, "_");
            
            if (!filters.hasOwnProperty(col)) filters[col] = [ ];
            
            if (Array.isArray(row[col])) {
                row[col].forEach(function(val) {
                    if (filters[col].indexOf(val) === -1)
                        filters[col].push(val);
                });
            } else {
                if (row.hasOwnProperty(col) &&
                    filters[col].indexOf(row[col] === -1)) {
                    filters[col].push(row[col]);
                }
            }
            
            filters[col] = filters[col].sort();
        });
    });
    
    return filters;
};

module.exports = dataProvider;