/**
 * yyz/client/services/markdown.service.js
 * 
 * A custom markdown parser/formatter.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/09/2016
 */
var format;

module.exports = ["$sce", function($sce) {
    return {
        "format": function(flags, input) {
            return $sce.trustAsHtml(format(flags, input));
        }
    };
}];

format = function(flags, input) {
    var output = input;
    
    if (typeof input === "string") {
        // If we don't find the 'noEscapeMarkup' flag, then we'll need to
        //  "escape" less-than and greater-than signs, since those can be
        //  used to signify the start and end of markup elements.
        if (flags.indexOf("noEscapeMarkup") === -1) {
            output = output.replace(/(\<)/gi, "&lt;");
            output = output.replace(/(\>)/gi, "&gt;");
        }
        
        // If we don't find the 'noNewlines' flag, then we'll go ahead and
        //  common line-break characters, with the 'br' HTML-element.
        if (flags.indexOf("noNewlines") === -1) {
            output = output.replace(/(\n|\r\f)/gi, "<br>");
        }
        
        // If we don't find the 'noHyperlinks' flag, then we'll go ahead and
        //  replace plain-text links, with a hyperlink.
        //
        // TODO: Figure out a good way to support markdown links, alongside
        //  plain-text links.
        if (flags.indexOf("noHyperlinks") === -1) {
            // output = output.replace(/\[([\w\s]+)\]\((https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))\)/gi, "<a href='$2' target='_blank'>$1</a>");
            output = output.replace(/(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))/gi, "<a href='$1' target='_blank'>$1</a>");
        }
        
        // Bold
        // output = output.replace(/\*\*([\W\w^(\<br\>)]+)\*\*/gi, "<span style='font-weight:bold;'>$1</span>");
        
        // Italic
        // output = output.replace(/\_([\W\w^(\<br\>)]+)\_/gi, "<span style='font-style=italic;'>$1</span>");
        
        // Strikeout
        // output = output.replace(/\~{2}([\W\w^(\<br\>)]+)\~{2}/gi, "<span style='text-decoration:line-through;'>$1</span>");
        
        // TODO: Inline and block code
    }
    
    return (output + " ");
};