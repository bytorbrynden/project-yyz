/**
 * yyz/server/data_providers/main.js
 * 
 * A "map" of all data-providers that Project YYZ contains.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
module.exports = {
    "json": require("./json.provider.js"),
    "sqlite": require("./sqlite.provider.js")
};