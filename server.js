/**
 * yyz/server.js
 * 
 * The entry-point for the yyz application.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/05/2016
 */
var express = require("express");
var path    = require("path");
var router  = require("./server/router.js");
var app     = express();
var port    = process.env.port || process.env.PORT || 25565;

// Request "handling" middleware that is called upon for every request to server
app.use(function(request, response, next) {
    var thisDate = new Date();
    
    var dateStr = thisDate.getMonth() + "/" + thisDate.getDate() + "/" +
        thisDate.getFullYear();
    var timeStr = thisDate.getHours() + ":" + thisDate.getMinutes() + ":" +
        thisDate.getSeconds();
    var now = dateStr + " " + timeStr;
    
    // Log a message for every request to the server
    console.log("[%s] %s request to '%s' from '%s'", now, request.method,
        request.path, request.ip);
    
    // Add an 'X-Handled-By' header to every response from the server
    response.set("X-Handled-By", "YYZ Server");
    
    next();
});

// Error "handling" middleware that is called upon any time the server encounters
//  an error
app.use(function(error, request, response, next) {
    response.sendStatus(error.status ? error.status : 500);
    
    // We really shouldn't do this, but we still do, to make ESLint happy
    //  (see ESLint 'no-unused-vars' rule)
    next();
});

app.use("/client", express.static(path.resolve("./client")));

// Setup server routes
app.use(router(app));

app.listen(port, function() {
    console.log("Server running on port %s", port);
});