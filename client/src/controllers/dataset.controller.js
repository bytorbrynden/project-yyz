/**
 * yyz/client/controllers/dataset.controller.js
 * 
 * Defines our roodatasett controller's logic.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 05/03/2016
 */
module.exports = ["$http", "$scope", function($http, $scope) {
    $scope.dataset = { };
    $scope.loaded  = false;
    
    $http({"method": "GET", "url": window.yyzApiUrl}).then(
        function(response) {
            $scope.dataset = response.data;
            $scope.loaded  = true;
            
            console.info("Dataset name: %s", $scope.dataset.name);
        },
        function(error) {
            console.error(error);
        }
    );
}];