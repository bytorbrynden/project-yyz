/**
 * yyz/server/router.js
 * 
 * The entry-point for our "custom" router.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var express = require("express");
var router = express.Router();

module.exports = function(app) {
    // Setup api routes
    app.use("/api", require("./routes/api.js"));
    
    // Setup client routes
    app.use("/client", require("./routes/client.js"));
    
    router.get("/", function(request, response) {
        // If a user makes a GET request to the root of the server, they're
        //  likely looking for the client, so we'll redirect them.
        response.redirect("/client");
    });
    
    return router;
};