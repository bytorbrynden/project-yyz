/**
 * yyz/gulpfile.js
 * 
 * Defines all of our gulp tasks.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/25/2016
 */
var gulp       = require("gulp");
var path       = require("path");
var spawn      = require("child_process").spawn;
var eslint     = require("gulp-eslint");
var source     = require("vinyl-source-stream");
var browserify = require("browserify");

var server = null;

var eslintOpts = { "config": "eslint.json" };

var serverScripts = path.resolve("./server/**/*.js");
var clientScripts = path.resolve("./client/src/**/*.js");

var serverWatches = [ serverScripts ];
var clientWatches = [ clientScripts ];

gulp.task("default", ["client", "server"], function() {
    gulp.watch(serverWatches, ["server"]);
    gulp.watch(clientWatches, ["client"]);
});

gulp.task("server", ["lint-server", "run-server"]);
gulp.task("client", ["lint-client", "bundle-client"]);

gulp.task("lint-server", function() {
    return gulp.src(serverScripts)
        .pipe(eslint(eslintOpts)).pipe(eslint.format());
});

gulp.task("lint-client", function() {
    return gulp.src(clientScripts)
        .pipe(eslint(eslintOpts)).pipe(eslint.format());
});

gulp.task("bundle-client", function() {
    return browserify(path.resolve("./client/src/app.js")).bundle()
        .pipe(source("app.js"))
        .pipe(gulp.dest("./client/dist"));
});

gulp.task("run-server", function() {
    if (server !== null) {
        server.kill();
    }
    
    server = spawn("node", [ path.resolve("./server.js") ], {
        "stdio": "inherit"
    })
});

process.on("exit", function() {
    if (server !== null) {
        server.kill();
    }
});