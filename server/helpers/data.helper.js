/**
 * yyz/server/helpers/data.helper.js
 * 
 * Contains all data-related helper logic.
 * Created by Brynden "Gigabyte Giant" Bielefeld on 04/28/2016
 */
var fs      = require("fs");
var request = require("request");

var _getData = function(path, meta, next) {
    if (meta.dataType === "json") {
        if (meta.locationType === "local") {
            next(JSON.parse(fs.readFileSync(path)));
        } else if (meta.locationType === "external") {
            request(path, function(error, response, body) {
                if (error) {
                    console.error(error);
                    
                    return next({});
                }
                
                if (response.statusCode === 200) {
                    next(JSON.parse(body));
                }
            });
        }
    } else if (meta.dataType === "sqlite") {
        next(fs.readFileSync(path));
    }
};

module.exports = function() {
    return {
        "getData": _getData
    };
};